import 'dart:convert';

import 'package:pedidos_app/scr/models/user.model.dart';
import 'package:pedidos_app/scr/models/token.model.dart';
import 'package:pedidos_app/scr/models/product.model.dart';
import 'package:pedidos_app/scr/models/shopcart.model.dart';
import 'package:pedidos_app/scr/models/restaurat.model.dart';
import 'package:pedidos_app/scr/models/direction.model.dart';

class Constants {

  static final String REQUIRED = 'Campo requerido';
  static final String URL_DEBUG = 'http://143.198.57.168:3000';
  static final String URL_GET_TOKEN = URL_DEBUG + '/users/token'; //GET
  static final String URL_REGISTER = URL_DEBUG + '/users/register'; //POST
  static final String URL_LOGIN = URL_DEBUG + '/users/login'; //POST
  static final String URL_PRODUCT_GET = URL_DEBUG + '/product/list/up'; //POST
  static final String URL_SHOP_GET = URL_DEBUG + '/shop/list'; //GET
  static final String URL_ORDER_SEND = URL_DEBUG + '/order/save'; //POST
  static final String URL_PROMO_GET = URL_DEBUG + '/promo/list'; //POST
  static final String URL_DIR_REG = URL_DEBUG + '/users/add/address'; //POST
  static final String URL_DIR_GET = URL_DEBUG + '/users/address'; //POST
  static final String DENIIS = URL_DEBUG + 'Fernando1009'; //POST

  static List<ShopcartModel> shopcart = [];
  static String msg = 'Error de conexión.';
  static bool auxRoute;
  static Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Authorization':
    'Basic ' + base64.encode(utf8.encode('rapinasa_movil:@ppM0v1l#access'))
  };

  static UserModel user;
  static TokenModel token;
  static DirectionModel dir;
  static ProductModel product;
  static ListProducts products;
  static RestaurantModel restaurant;
  static ListRestaurants restaurants;
}
