import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:pedidos_app/scr/models/user.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/models/direction.model.dart';

class Utilities {

  static List<DirectionModel> getDirs() {
    List<DirectionModel> dirs = [];
    dirs.add(DirectionModel(id: '1', mainStreet: '21 de Septiembre', secoundStreet: 'Luis A. Alvear', hoseNumber: '02 - 10', reference: 'Junto a la escuela',));
    dirs.add(DirectionModel(id: '2', mainStreet: 'Gonzalo Montes de Oca', secoundStreet: 'Geovanny Calle', hoseNumber: '67 - 473', reference: 'Cerva del CRV',));
    return dirs;
  }

  static double totalOrder() {
    double total = 0;
    Constants.shopcart.map((shopcart) {
      print(shopcart.product.cost);
      total += shopcart.cant * shopcart.product.cost;
    }).toList();
    return total;
  }

  static String valField(String value) {
    String msg;
    if (value.isEmpty) msg = Constants.REQUIRED;
    return msg;
  }

  static String valNUI(String nui) {
    String msg;
    var total = 0;
    var long = nui.length;
    print(nui.isEmpty);
    print(long);
    var longCheck = long - 1;
    var decimo = int.parse(nui.split('').last);
    if (nui.isNotEmpty && long == 10) {
      for (int i=0; i<longCheck; i++) {
        if (i%2 == 0) {
          int aux = int.parse(nui.split('')[i])*2;
          if (aux > 9) aux -= 9;
          total += aux;
        } else {
          total += int.parse(nui.split('')[i]);
        }
      }
      total = total%10 != 10? 10 - total%10:0;
      if (decimo != total) {
        print('Cédula incorrecta');
        msg = 'Verifique su número de cédula';
      } else
        print('Cédula correcta');
    } else {
      msg = 'Verifique su número de cédula';
    }
    return msg;
  }

  static String validateEmail(String value) {
    String msg;
    if (value.isEmpty)
      msg = 'Campo requerido';
    else {
      bool band = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(value);
      if (!band) msg = 'Verifique su correo';
    }
    return msg;
  }

  static ImageProvider getUserImage(String url) {
    if (url != null && url.isNotEmpty) {
      return NetworkImage(url);
    } else
      return AssetImage('assets/images/user_desfault.png');
  }

  static ImageProvider getPetImage(String url, String type) {
    if (url != null && url.isNotEmpty)
      return NetworkImage(url);
    else {
      if (type == 'dog')
        return AssetImage('assets/images/img_dog.png');
      else
        return AssetImage('assets/images/img_cat.png');
    }
  }

  static saveSession(UserModel user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String id = user.id;
    String email = user.account.email;
    String password = user.account.password;
    String dni = user.dni;
    bool state = user.active;
    String firstName = user.firstName;
    String lastNames = user.lastName;
    String photo = user.photo;
    await pref.setString('email', email);
    await pref.setString('password', password);
    await pref.setString('id', id);
    await pref.setString('dni', dni);
    await pref.setBool('state', state);
    await pref.setString('firstName', firstName);
    await pref.setString('lastNames', lastNames);
    await pref.setString('photo', photo);
  }

  static Future<UserModel> getSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    UserModel person = UserModel(
      id: pref.getString('id'),
      dni: pref.getString('dni'),
      firstName: pref.getString('firstName'),
      lastName: pref.getString('lastName'),
      active: pref.getBool('state'),
      photo: pref.getString('photo'),
    );
    return person;
  }

  static List<String> getDateString(String date) {
    String dateAux = date.split('T')[0];
    String timeAux = date.split('T')[1].split(':')[0] +
        ':' +
        date.split('T')[1].split(':')[1];
    String mount = getMount(dateAux.split('-')[1]);
    String aux = dateAux.split('-')[2] + mount + dateAux.split('-')[0];
    return [timeAux, aux];
  }

  static String getMount(String date) {
    String mount = "";
    switch (date) {
      case "01":
        mount = "ENE";
        break;
      case "02":
        mount = "FEB";
        break;
      case "03":
        mount = "MAR";
        break;
      case "04":
        mount = "ABR";
        break;
      case "05":
        mount = "MAY";
        break;
      case "06":
        mount = "JUN";
        break;
      case "07":
        mount = "JUL";
        break;
      case "08":
        mount = "AGO";
        break;
      case "09":
        mount = "SEP";
        break;
      case "10":
        mount = "OCT";
        break;
      case "11":
        mount = "NOV";
        break;
      case "12":
        mount = "DIC";
        break;
    }
    return mount;
  }
}
