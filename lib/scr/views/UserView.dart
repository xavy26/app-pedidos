import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pedidos_app/scr/models/user.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';

class UserView extends StatefulWidget {
  @override
  _UserViewState createState() => _UserViewState();
}

class _UserViewState extends State<UserView> {

  UserModel user;
  String photoUrl;

  @override
  void initState() {
    super.initState();
    user = Constants.user;
    photoUrl = user.photo;
  }
  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/register/user');
        },
        child: Icon(
          Icons.edit,
          color: Colors.white,
        ),
      ),
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 32,
          ),
          children: [
            Center(
              child: Container(
                width: size.width / 3,
                height: size.width / 3,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100.0),
                  color: Colors.white,
                  image: DecorationImage(
                    image: MemoryImage(base64Decode(photoUrl)),
                    fit: BoxFit.fitWidth
                  )
                ),
              ),
            ),
            SizedBox(height: 32),
            Text(
              '${user.firstName} ${user.lastName}',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline5,
            ),
            SizedBox(height: 16),
            Text(
              '${user.account.email}',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline6,
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '${user.typeDni}: ',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6,
                ),
                SizedBox(width: 8.0),
                Text(
                  '${user.dni}',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ],
            ),
            SizedBox(height: 32),
            TextButton(
              onPressed: () => Navigator.pushNamed(context, '/directions'),
              child: Text(
                'Direcciones',
                style: GoogleFonts.montserrat(
                  fontSize: 18,
                  color: Theme.of(context).accentColor
                ),
              )
            )
          ],
        ),
      ),
    );
  }

}
