import 'dart:async';

import 'package:flutter/material.dart';

import 'package:pedidos_app/scr/controllers/user.controller.dart';

class SplashScreenView extends StatefulWidget {

  @override
  _SplashScreenViewState createState() => _SplashScreenViewState();
}

class _SplashScreenViewState extends State<SplashScreenView> {

  @override
  void initState() {
    super.initState();
    UserController.getToken(context);
    Timer(Duration(seconds: 3), () {
      Navigator.pushReplacementNamed(context, '/init');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Theme.of(context).primaryColor,
              Theme.of(context).accentColor
            ]
          )
        ),
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: [
              Container(
                width: MediaQuery.of(context).size.width/3,
                height: MediaQuery.of(context).size.width/3,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/logo_app.png')
                  )
                ),
              ),
              SizedBox(height: 16),
              Text(
                'RapiNasa',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline4,
              ),
              SizedBox(height: 16),
              Container(
                width: 64,
                height: 64,
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  strokeWidth: 4,
                  valueColor: AlwaysStoppedAnimation<Color>(
                    Theme.of(context).primaryColorDark,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
