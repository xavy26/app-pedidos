import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:pedidos_app/scr/models/shopcart.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/views/Widgets/ShopCartItem.dart';

class ShopcartView extends StatefulWidget {
  @override
  _ShopcartViewState createState() => _ShopcartViewState();
}

class _ShopcartViewState extends State<ShopcartView> {
  List<ShopcartModel> shopcart;

  @override
  void initState() {
    super.initState();
    shopcart = Constants.shopcart;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Carrito', style: Theme.of(context).textTheme.headline3),
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                shopcart.clear();
                Constants.shopcart.clear();
              });
            },
            icon: Icon(
              Icons.remove_shopping_cart,
              color: Theme.of(context).primaryColorDark,
            )
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).accentColor,
        child: Icon(
          Icons.arrow_forward_ios,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pushNamed(context, '/select/dir');
        },
      ),
      body: shopcart.isEmpty
          ? Center(
              child: Text(
                'EL carrito está vacío!',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6,
              ),
            )
          : ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 16.0, left: 8.0, right: 8.0),
              itemCount: shopcart.length,
              itemBuilder: (context, index) {
                return ShopCartItem(shopcart[index], index);
              },
            ),
    );
  }
}
