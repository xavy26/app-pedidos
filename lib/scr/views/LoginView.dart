import 'package:flutter/material.dart';
import 'package:pedidos_app/scr/controllers/product.controller.dart';
import 'package:pedidos_app/scr/controllers/user.controller.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/utilities/Utilities.dart';
import 'package:toast/toast.dart';

class LoginView extends StatefulWidget {

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {

  final _formKey = GlobalKey<FormState>();

  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();

  bool _obscured = true;
  void _toggle() {
    setState(() {
      _obscured = !_obscured;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 16),
            children: [
              Text(
                'Ingresar',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline2,
              ),
              SizedBox(height: 32),
              TextFormField(
                controller: _email,
                validator: (value) => Utilities.validateEmail(value),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: 'Correo electrónico',
                ),
              ),
              SizedBox(height: 16.0),
              TextFormField(
                controller: _password,
                obscureText: _obscured,
                validator: (value) => Utilities.valField(value),
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  labelText: 'Contraseña',
                  suffixIcon: IconButton(
                    icon: Icon(
                      _obscured? Icons.visibility_off : Icons.visibility,
                      color: Colors.black45,
                    ),
                    onPressed: _toggle
                  ),
                ),
              ),
              SizedBox(height: 32.0),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 64.0,
                    vertical: 16.0
                ),
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      UserController.login(
                          context: context,
                          email: _email.text,
                          password: _password.text
                      );
                    } else {
                      Toast.show('Verifique que todos los campos erroneas', context, duration: Toast.LENGTH_LONG);
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 10.0,
                    ),
                    child: Text(
                      'INGRESAR',
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
