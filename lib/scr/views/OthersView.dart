import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class OthersView extends StatefulWidget {

  @override
  _OthersViewState createState() => _OthersViewState();
}

class _OthersViewState extends State<OthersView> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ListTile(
          title: Text('Cerrar sesión'),
          leading: Icon(
            Icons.exit_to_app,
            color: Theme.of(context).accentColor,
          ),
          onTap: () {
            Toast.show('Cerrando sesión...', context, duration: Toast.LENGTH_LONG);
            Navigator.pushNamedAndRemoveUntil(context, '/init', (route) => false);
          },
        ),
        Divider(
          color: Colors.black26,
        ),
        ListTile(
          title: Text('Tengo restaurant'),
          leading: Icon(
            Icons.restaurant,
            color: Theme.of(context).accentColor,
          ),
          onTap: () {
            Navigator.pushNamed(context, '/register/restaurant');
          },
        ),
        Divider(
          color: Colors.black26,
        ),
        ListTile(
          title: Text('Condiciones y políticas'),
          leading: Icon(
            Icons.text_snippet_outlined,
            color: Theme.of(context).accentColor,
          ),
          onTap: () {
            print('Condiciones y políticas');
          },
        ),
        Divider(
          color: Colors.black26,
        ),
        ListTile(
          title: Text('Ayuda'),
          leading: Icon(
            Icons.help_outline,
            color: Theme.of(context).accentColor,
          ),
          onTap: () {
            print('Ayuda');
          },
        ),
        Divider(
          color: Colors.black26,
        ),
        ListTile(
          title: Text('Acerca de'),
          leading: Icon(
            Icons.info_outline,
            color: Theme.of(context).accentColor,
          ),
          onTap: () {
            print('Acerca de');
          },
        ),
        Divider(
          color: Colors.black26,
        ),
      ],
    );
  }
}
