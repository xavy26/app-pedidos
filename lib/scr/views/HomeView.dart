import 'package:flutter/material.dart';

import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import 'package:pedidos_app/scr/views/UserView.dart';
import 'package:pedidos_app/scr/views/OthersView.dart';
import 'package:pedidos_app/scr/views/RestaurantsView.dart';

class HomeView extends StatefulWidget {

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {

  int _page = 0;
  bool _restBand = true;
  bool _userBand = false;
  bool _othersBand = false;
  SearchBar _searchBar;
  final UserView _userView = UserView();
  final OthersView _othersView = OthersView();
  final RestaurantsView _restaurantsView = RestaurantsView();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
      title: new Text(
        'RapiNASA',
        style: Theme.of(context).textTheme.headline3
      ),
      leading: _searchBar.getSearchAction(context),
      actions: [
        IconButton(
          icon: Icon(
            Icons.shopping_cart,
            color: Theme.of(context).primaryColorDark,
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/shopcart');
          },
        ),
      ],
    );
  }

  void onSubmitted(String value) {
    setState(() {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('You wrote $value')
        )
      );
    });
  }

  _HomeViewState() {
    _searchBar = SearchBar(
      inBar: false,
      setState: setState,
      onSubmitted: onSubmitted,
      buildDefaultAppBar: buildAppBar,
      onCleared: () {
        print('cleared');
      },
      onClosed: () {
        print('closed');
      }
    );
  }

  Widget _showPage = RestaurantsView();

  Widget _pageChoose(int index) {
    Widget page;
    switch (index) {
      case 0:
        _restBand = true;
        _userBand = false;
        _othersBand = false;
        page = _restaurantsView;
        break;
      case 1:
        _restBand = false;
        _userBand = true;
        _othersBand = false;
        page =  _userView;
        break;
      case 2:
        _restBand = false;
        _userBand = false;
        _othersBand = true;
        page =  _othersView;
        break;
      default:
        _restBand = true;
        _userBand = false;
        _othersBand = false;
        page = _restaurantsView;
    }
    return page;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: _searchBar.build(context),
      bottomNavigationBar: CurvedNavigationBar(
        height: 50,
        index: _page,
        backgroundColor: Colors.white,
        animationCurve: Curves.bounceInOut,
        color: Theme.of(context).accentColor,
        animationDuration: Duration(milliseconds: 200),
        items: [
          Icon(
            Icons.home,
            size: 30,
            color: _restBand?Colors.white:Theme.of(context).primaryColorDark
          ),
          Icon(
            Icons.person,
            size: 30,
            color: _userBand?Colors.white:Theme.of(context).primaryColorDark
          ),
          Icon(
            Icons.menu,
            size: 30,
            color: _othersBand?Colors.white:Theme.of(context).primaryColorDark
          ),
        ],
        onTap: (index) {
          setState(() {
            _page = index;
            _showPage = _pageChoose(_page);
          });
        }
      ),
      body: Container(
        child: _showPage
      ),
    );
  }
}
