import 'package:flutter/material.dart';

import 'package:pedidos_app/scr/models/shopcart.model.dart';

import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/models/product.model.dart';
import 'package:pedidos_app/scr/views/Widgets/ProductItem.dart';
import 'package:pedidos_app/scr/views/Widgets/ProductDetail.dart';
import 'package:pedidos_app/scr/views/Widgets/RateRest.dart';

class ProductView extends StatefulWidget {

  @override
  _ProductViewState createState() => _ProductViewState();
}

class _ProductViewState extends State<ProductView> {

  List<ProductModel> products;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    products = Constants.restaurant.products.products;
  }

  void prodDetail(ProductModel product) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return ProductDetail(product: product);
      },
    );
  }

  void rateRest() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return RateRest(Constants.restaurant);
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${Constants.restaurant.name}',
          style: Theme.of(context).textTheme.headline3
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Theme.of(context).primaryColorDark,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/shopcart');
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          Stack(
            alignment: Alignment.bottomRight,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height/4,
                decoration: BoxDecoration(
                  color: Colors.black26,
                  image: DecorationImage(
                    image: NetworkImage('${Constants.restaurant.imgProfile}'),
                    fit: BoxFit.fill
                  )
                ),
              ),
              InkWell(
                onTap: () {
                  rateRest();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      Icon(
                        Icons.star_half,
                        color: Colors.amber,
                      ),
                      Icon(
                        Icons.star_border,
                        color: Colors.amber,
                      ),
                      Icon(
                        Icons.star_border,
                        color: Colors.amber,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.only(
                top: 16.0,
                left: 8.0,
                right: 8.0
            ),
            itemCount: products.length,
            itemBuilder: (context, index) {
              return InkWell(
                child: ProductItem(item: new ShopcartModel(cant: 0, product: products[index])),
                onTap: () {
                  prodDetail(products[index]);
                },
              );
            },
          ),
        ],
      )
    );
  }
}
