import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import 'package:pedidos_app/scr/utilities/Utilities.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/models/direction.model.dart';
import 'package:pedidos_app/scr/controllers/user.controller.dart';

class RegisterDirView extends StatefulWidget {
  @override
  _RegisterDirViewState createState() => _RegisterDirViewState();
}

class _RegisterDirViewState extends State<RegisterDirView> {

  final _formKey = GlobalKey<FormState>();
  TextEditingController _name = TextEditingController();
  TextEditingController _canton = TextEditingController();
  TextEditingController _country = TextEditingController();
  TextEditingController _province = TextEditingController();
  TextEditingController _reference = TextEditingController();
  TextEditingController _mainStreet = TextEditingController();
  TextEditingController _houseNumber = TextEditingController();
  TextEditingController _secoundStreet = TextEditingController();

  DirectionModel getDirection() {
    return DirectionModel(
      name: _name.text,
      country: _country.text,
      province: _province.text,
      canton: _canton.text,
      reference: _reference.text,
      mainStreet: _mainStreet.text,
      hoseNumber: _houseNumber.text,
      secoundStreet: _secoundStreet.text
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            padding: const EdgeInsets.only(
              top: 64.0,
              left: 32.0,
              right: 32.0,
              bottom: 8.0
            ),
            children: [
              Container(
                width: size.width / 7,
                height: size.width / 7,
                child: Image.asset('assets/images/img_dir.png'),
              ),
              SizedBox(height: 16),
              TextFormField(
                controller: _name,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Nombre'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _country,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Pais'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _province,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Provincia'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _canton,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Canton'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _mainStreet,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Calle principal'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _secoundStreet,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Calle secundaria'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _houseNumber,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Número de casa'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _reference,
                keyboardType: TextInputType.text,
                validator: (value) => Utilities.valField(value),
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(labelText: 'Referencia'),
              ),
              SizedBox(height: 32),
              FloatingActionButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    UserController.regDir(context, getDirection()).then((dir) {
                      setState(() {
                        if (!Constants.auxRoute)
                          Constants.user.addresses.add(dir);
                      });
                      if (Constants.auxRoute)
                        Navigator.pushNamedAndRemoveUntil(context, '/login', (route) => false);
                      else
                        Navigator.pop(context);
                    });
                  } else {
                    Toast.show('Verifique que todos los campos erroneas', context, duration: Toast.LENGTH_LONG);
                  }
                },
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
