import 'package:flutter/material.dart';
import 'package:pedidos_app/scr/models/restaurat.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';

import 'package:pedidos_app/scr/views/Widgets/RestItem.dart';

class RestaurantsView extends StatefulWidget {

  @override
  _RestaurantsViewState createState() => _RestaurantsViewState();
}

class _RestaurantsViewState extends State<RestaurantsView> {

  List<RestaurantModel> restaurants = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      restaurants = Constants.restaurants.restaurants;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView.builder(
        itemCount: restaurants.length,
        itemBuilder: (context, index) {
          return InkWell(
            child: RestItem(restaurant: restaurants[index]),
            onTap: () {
              Constants.restaurant = restaurants[index];
              Navigator.pushNamed(context, '/product');
            },
          );
        },
      ),
    );
  }
}
