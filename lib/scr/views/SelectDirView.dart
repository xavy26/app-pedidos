import 'package:flutter/material.dart';

import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/utilities/Utilities.dart';
import 'package:pedidos_app/scr/models/direction.model.dart';
import 'package:pedidos_app/scr/views/Widgets/DirectionItem.dart';

class SelectDirView extends StatefulWidget {
  const SelectDirView({Key key}) : super(key: key);

  @override
  _SelectDirViewState createState() => _SelectDirViewState();
}

class _SelectDirViewState extends State<SelectDirView> {

  List<DirectionModel> dirs;

  @override
  void initState() {
    super.initState();
    dirs = Constants.user.addresses;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Orden',
          style: Theme.of(context).textTheme.headline3
        ),
      ),
      body: ListView.builder(
        shrinkWrap: true,
        itemCount: dirs.length,
        itemBuilder: (context, index) {
          return InkWell(
            child: DirectionItem(dir: dirs[index]),
            onTap: () {
              setState(() {
                Constants.dir = dirs[index];
              });
              Navigator.pushNamed(context, '/detail');
            },
          );
        },
      ),
    );
  }
}
