import 'dart:io';
import 'dart:convert';

import 'package:pedidos_app/scr/controllers/user.controller.dart';
import 'package:toast/toast.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import 'package:pedidos_app/scr/models/user.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/utilities/Utilities.dart';
import 'package:pedidos_app/scr/models/account.model.dart';

class RegisterUserView extends StatefulWidget {
  @override
  _RegisterUserViewState createState() => _RegisterUserViewState();
}

enum SingingCharacter { Hombre,Mujer }

class _RegisterUserViewState extends State<RegisterUserView> {

  File _image;
  String bs64;
  String gender = 'Hombre';
  Widget _photoUser = Image.asset('assets/images/add_photo_user.png');

  final picker = ImagePicker();
  final _formKey = GlobalKey<FormState>();
  SingingCharacter _sex = SingingCharacter.Hombre;

  bool _obscured = true;
  TextEditingController _nui = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _birthday = TextEditingController();
  TextEditingController _lastName = TextEditingController();
  TextEditingController _firstName = TextEditingController();

  UserModel getUser() {
    AccountModel account = AccountModel();
    UserModel person = UserModel();
    account.email = _email.text;
    account.password = _password.text;
    person.account = account;
    person.firstName = _firstName.text;
    person.lastName = _lastName.text;
    person.typeDni = 'Cédula';
    person.dni = _nui.text;
    person.gender = gender;
    person.birthdate = _birthday.text;
    person.type = 'Usuario';
    if (_image != null) {
      person.photo = bs64;
    }
    print('Gender: ${person.gender}');
    return person;
  }

  void _toggle() {
    setState(() {
      _obscured = !_obscured;
    });
  }

  Future _openCamera() async {
    var picture = await picker.getImage(
      imageQuality: 50,
      source: ImageSource.camera,
    );
    if (picture != null) {
      _image = File(picture.path);
      setState(() {
        _photoUser = Container(
          width: 150,
          height: 150,
          child: CircleAvatar(
            backgroundImage: FileImage(_image),
          ),
        );
        _image = File(picture.path);
      });
      List<int> bytes = await _image.readAsBytes();
      bs64 = base64Encode(bytes);
    }
  }

  Future _openGallery() async {
    var picture = await picker.getImage(
      imageQuality: 50,
      source: ImageSource.gallery,
    );
    if (picture != null) {
      _image = File(picture.path);
      _photoUser = Image.file(_image);
      setState(() {
        _photoUser = Container(
          width: 150,
          height: 150,
          child: CircleAvatar(
            backgroundImage: FileImage(_image),
          ),
        );
      });
      List<int> bytes = await _image.readAsBytes();
      bs64 = base64Encode(bytes);
    }
  }

  Future<void> _dialogPhoto(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(
                "Selecciona foto de:",
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 20),
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    ListTile(
                      title: Text("Cámara"),
                      leading: Icon(
                        Icons.camera_alt,
                        color: Colors.black45,
                      ),
                      onTap: () {
                        _openCamera();
                        Navigator.pop(context);
                      },
                    ),
                    ListTile(
                      title: Text("Galería"),
                      leading: Icon(
                        Icons.image,
                        color: Colors.black45,
                      ),
                      onTap: () {
                        _openGallery();
                        Navigator.pop(context);
                      },
                    ),
                    ListTile(
                      title: Text("Cancelar"),
                      leading: Icon(
                        Icons.close,
                        color: Colors.black45,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ));
        });
  }

  Future<void> _showDataPicker() {
    DatePicker.showDatePicker(
      context,
      locale: LocaleType.es,
      showTitleActions: true,
      maxTime: DateTime.now(),
      minTime: DateTime(1950, 1, 1),
      theme: DatePickerTheme(
        headerColor: Theme.of(context).accentColor,
        backgroundColor: Theme.of(context).primaryColor,
        cancelStyle: TextStyle(
          color: Colors.white
        ),
        doneStyle: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold
        )
      ),
      onConfirm: (date) {
        String birthday = '${date.year}-${date.month}-${date.day}';
        setState(() {
          _birthday.text = birthday;
        });
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            padding: const EdgeInsets.symmetric(
              vertical: 8,
              horizontal: 32,
            ),
            shrinkWrap: true,
            children: [
              Container(
                width: size.width / 3,
                height: size.width / 3,
                child: GestureDetector(
                  child: _photoUser,
                  onTap: () => _dialogPhoto(context),
                ),
              ),
              SizedBox(height: 32),
              Row(
                children: [
                  Text(
                    'Genero:',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  Radio(
                    value: SingingCharacter.Hombre,
                    groupValue: _sex,
                    onChanged: (SingingCharacter value) {
                      setState(() {
                        _sex = value;
                        gender = _sex.toString().split('.').last;
                      });
                    },
                  ),
                  Text(
                      'Masculino'
                  ),
                  Radio(
                    value: SingingCharacter.Mujer,
                    groupValue: _sex,
                    onChanged: (SingingCharacter value) {
                      setState(() {
                        _sex = value;
                        gender = _sex.toString().split('.').last;
                      });
                    },
                  ),
                  Text(
                      'Femenino'
                  ),
                ],
              ),
              SizedBox(height: 8),
              TextFormField(
                maxLength: 10,
                controller: _nui,
                validator: (value) => Utilities.valNUI(value),
                keyboardType: TextInputType.numberWithOptions(
                  signed: false,
                  decimal: false
                ),
                decoration: InputDecoration(labelText: 'Número de cédula'),
              ),
              SizedBox(height: 4),
              TextFormField(
                controller: _firstName,
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                validator: (value) => Utilities.valField(value),
                decoration: InputDecoration(labelText: 'Nombre'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _lastName,
                keyboardType: TextInputType.name,
                textCapitalization: TextCapitalization.words,
                validator: (value) => Utilities.valField(value),
                decoration: InputDecoration(labelText: 'Apellido'),
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _birthday,
                decoration: InputDecoration(
                  labelText: 'Fecha de cumpleaños',
                  suffixIcon: IconButton(
                    icon: Icon(
                      Icons.date_range,
                      color: Colors.black45,
                    ),
                    onPressed: _showDataPicker,
                  ),
                ),
                onTap: _showDataPicker,
              ),
              SizedBox(height: 8),
              TextFormField(
                controller: _email,
                keyboardType: TextInputType.emailAddress,
                validator: (value) => Utilities.validateEmail(value),
                decoration: InputDecoration(labelText: 'Correo electrónico'),
              ),
              SizedBox(height: 8),
              TextFormField(
                obscureText: _obscured,
                controller: _password,
                keyboardType: TextInputType.visiblePassword,
                validator: (value) => Utilities.valField(value),
                decoration: InputDecoration(
                  labelText: 'Contraseña',
                  suffixIcon: IconButton(
                    icon: Icon(
                      _obscured ? Icons.visibility_off : Icons.visibility,
                      color: Colors.black45,
                    ),
                    onPressed: _toggle
                  ),
                ),
              ),
              SizedBox(height: 32),
              FloatingActionButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    UserController.register(context, getUser()).then((user) {
                      if (user != null) {
                        Constants.user = user;
                        Constants.auxRoute = true;
                        Navigator.pushNamed(context, '/register/dir');
                      }
                    });
                  } else {
                    Toast.show('Verifique que todos los campos erroneas', context, duration: Toast.LENGTH_LONG);
                  }
                },
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}
