import 'package:flutter/material.dart';
import 'package:pedidos_app/scr/utilities/Utilities.dart';

class RegisterRestaurantView extends StatefulWidget {
  @override
  _RegisterRestaurantViewState createState() => _RegisterRestaurantViewState();
}

class _RegisterRestaurantViewState extends State<RegisterRestaurantView> {
  GlobalKey _formKey = GlobalKey<FormState>();
  TextEditingController _name = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Stack(
          children: [
            Container(
              width: size.width,
              height: size.width / 1.5,
              decoration: BoxDecoration(
                  color: Color(0xffcdcbcc),
                  image: DecorationImage(
                      image: AssetImage('assets/images/add_photo_user.png'))),
            ),
            ListView(
              padding: const EdgeInsets.symmetric(
                horizontal: 32,
              ),
              shrinkWrap: true,
              children: [
                SizedBox(height: size.width / 1.3),
                Container(
                  width: size.width / 3,
                  height: size.width / 3,
                  child: Image.asset('assets/images/add_photo_user.png'),
                ),
                SizedBox(height: 32),
                TextFormField(
                  controller: _name,
                  keyboardType: TextInputType.name,
                  textCapitalization: TextCapitalization.words,
                  validator: (value) => Utilities.valField(value),
                  decoration: InputDecoration(labelText: 'Nombre'),
                ),
                SizedBox(height: 16),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
