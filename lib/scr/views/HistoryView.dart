import 'package:flutter/material.dart';

class HistoryView extends StatefulWidget {

  @override
  _HistoryViewState createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Text(
          'HistoryView',
          style: Theme.of(context).textTheme.headline2,
        ),
      ),
    );
  }
}
