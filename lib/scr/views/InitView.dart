import 'package:flutter/material.dart';

import 'package:pedidos_app/scr/controllers/product.controller.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';

class InitView extends StatefulWidget {
  @override
  _InitViewState createState() => _InitViewState();
}

class _InitViewState extends State<InitView> {

  @override
  void initState() {
    super.initState();
    ProductController.getProducts(context).then((value) {
      setState(() {
        Constants.products = value;
        ProductController.getRestaurants(context).then((value) {
          setState(() {
            Constants.restaurants = value;
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.symmetric(horizontal: 32),
          children: [
            GestureDetector(
              onTap: () => Navigator.pushNamed(context, '/register/user'),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: size.width/4,
                    height: size.width/4,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/add_user_dark.png'),
                        fit: BoxFit.fitWidth
                      )
                    ),
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)
                          )
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/register/user');
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 24.0,
                          vertical: 10.0,
                        ),
                        child: Text(
                          'SOY NUEVO',
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Theme.of(context).primaryColorDark
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
            SizedBox(height: 96),
            GestureDetector(
              onTap: () => Navigator.pushNamed(context, '/login'),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: size.width/4,
                    height: size.width/4,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/user_dark.png'),
                        fit: BoxFit.fitWidth
                      )
                    ),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).accentColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)
                      )
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/login');
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 4.0,
                        vertical: 10.0,
                      ),
                      child: Text(
                        'TENGO CUENTA',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white
                        ),
                      ),
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
