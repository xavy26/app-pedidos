import 'package:flutter/material.dart';

import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/models/direction.model.dart';
import 'package:pedidos_app/scr/views/Widgets/DirectionItem.dart';

class DirectionsView extends StatefulWidget {
  @override
  _DirectionsViewState createState() => _DirectionsViewState();
}

class _DirectionsViewState extends State<DirectionsView> {

  List<DirectionModel> dirs;

  @override
  void initState() {
    super.initState();
    dirs = Constants.user.addresses;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Direcciones',
            style: Theme.of(context).textTheme.headline3
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Theme.of(context).accentColor,
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          onPressed: () {
            Constants.auxRoute = false;
            Navigator.pushNamed(context, '/register/dir');
          },
        ),
        body: ListView.builder(
          shrinkWrap: true,
          itemCount: dirs.length,
          itemBuilder: (context, index) {
            DirectionModel dir = dirs[index];
            print('Dir send: $dir');
            return DirectionItem(dir: dir);
          },
        )
    );
  }
}
