import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:pedidos_app/scr/controllers/product.controller.dart';

import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/utilities/Utilities.dart';

class OrderDetailView extends StatefulWidget {
  const OrderDetailView({Key key}) : super(key: key);

  @override
  _OrderDetailViewState createState() => _OrderDetailViewState();
}

class _OrderDetailViewState extends State<OrderDetailView> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Detalles',
          style: Theme.of(context).textTheme.headline3
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).accentColor,
        onPressed: () {
          ProductController.sendOrder(context);
        },
        child: Icon(
          Icons.send,
          color: Colors.white,
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0
        ),
        children: [
          SizedBox(height: 32.0),
          Text(
            'Detalles de su orden',
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
              fontSize: 22.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 32.0),
          Row(
            children: [
              Text(
                'Dirección:',
                style: GoogleFonts.montserrat(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(width: 16.0),
              SizedBox(
                width: MediaQuery.of(context).size.width/1.5,
                child: Text(
                  '${Constants.dir.mainStreet} (${Constants.dir.hoseNumber}) y ${Constants.dir.secoundStreet}',
                  maxLines: 3,
                  softWrap: true,
                  overflow: TextOverflow.clip,
                  style: GoogleFonts.montserrat(
                    fontSize: 16.0,
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 16.0),
          DataTable(
            columns: [
              DataColumn(label: Text('Producto')),
              DataColumn(label: Text('Cantidad')),
              DataColumn(label: Text('Precio')),
            ],
            rows: Constants.shopcart.map((e) => DataRow(
              cells: [
                DataCell(Text('${e.product.name}')),
                DataCell(Text('${e.cant}')),
                DataCell(Text('${e.product.cost}')),
              ]
            )).toList(),
          ),
          Divider(
            height: 8.0,
            color: Colors.black,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                'TOTAL:',
                style: GoogleFonts.montserrat(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                '${Utilities.totalOrder()}',
                softWrap: true,
                overflow: TextOverflow.clip,
                style: GoogleFonts.montserrat(
                  fontSize: 16.0,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
