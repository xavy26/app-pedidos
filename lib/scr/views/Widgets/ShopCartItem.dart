import 'package:flutter/material.dart';
import 'package:pedidos_app/scr/models/shopcart.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';

class ShopCartItem extends StatefulWidget {

  ShopcartModel item;
  int index;

  ShopCartItem(this.item, this.index);

  @override
  _ShopCartItemState createState() => _ShopCartItemState();
}

class _ShopCartItemState extends State<ShopCartItem> {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 75,
              height: 75,
              child: CircleAvatar(
                backgroundImage: NetworkImage('${this.widget.item.product.image}'),
              ),
            ),
            SizedBox(width: 8.0),
            Container(
              width: size.width/3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${this.widget.item.product.name}',
                    softWrap: true,
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).accentColor
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '\$ ${this.widget.item.product.cost}',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 8.0),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  border: Border.all(
                    width: 2,
                    color: Theme.of(context).primaryColorDark,
                  )
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    icon: Icon(
                        Icons.remove
                    ),
                    onPressed: () {
                      setState(() {
                        if (this.widget.item.cant >= 1) {
                          this.widget.item.cant--;
                          setState(() {
                            Constants.shopcart.removeAt(this.widget.index);
                          });
                        }
                      });
                    },
                  ),
                  Container(
                    width: 2,
                    height: 50,
                    color: Colors.black,
                    margin: const EdgeInsets.only(right: 16),
                  ),
                  Text(
                    '${this.widget.item.cant}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18
                    ),
                  ),
                  Container(
                    width: 2,
                    height: 50,
                    color: Colors.black,
                    margin: const EdgeInsets.only(left: 16),
                  ),
                  IconButton(
                    icon: Icon(
                        Icons.add
                    ),
                    onPressed: () {
                      setState(() {
                          this.widget.item.cant++;
                      });
                    },
                  ),
                ],
              ),
            )
          ],
        ),
        Divider(
          color: Colors.black26,
        )
      ],
    );
  }
}
