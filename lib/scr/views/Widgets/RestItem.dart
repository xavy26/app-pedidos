import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:pedidos_app/scr/models/restaurat.model.dart';

class RestItem extends StatelessWidget {

  RestaurantModel restaurant;

  RestItem({this.restaurant});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              width: 75,
              height: 75,
              margin: const EdgeInsets.all(16.0),
              child: CircleAvatar(
                backgroundColor: Color(0xFFF1F2F6),
                backgroundImage: NetworkImage('${restaurant.imgProfile}'),
              ),
            ),
            SizedBox(width: 16.0),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${restaurant.name}',
                  style: Theme.of(context).textTheme.headline1,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 4,
                  ),
                  decoration: BoxDecoration(
                      color: restaurant.state?Colors.green:Colors.redAccent,
                      borderRadius: BorderRadius.circular(5.0)
                  ),
                  child: Text(
                    restaurant.state?'Abierto':'Cerrado',
                    style: GoogleFonts.montserrat(
                        fontSize: 14,
                        color: Colors.white
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
        Divider(
          color: Colors.black26,
        ),
      ],
    );
  }
}
