import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pedidos_app/scr/models/direction.model.dart';

class DirectionItem extends StatelessWidget {

  DirectionModel dir;
  
  DirectionItem({this.dir});

  @override
  Widget build(BuildContext context) {
    print('Dir in: $dir');
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(
            vertical: 16
          ),
          child: Row(
            children: [
              Container(
                width: 100,
                height: 100,
                child: Image.asset('assets/images/img_dir.png'),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Principal:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor
                        ),
                      ),
                      SizedBox(width: 8),
                      Text(dir.mainStreet),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Secundaria:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor
                        ),
                      ),
                      SizedBox(width: 8),
                      Text(
                        dir.secoundStreet,
                        overflow: TextOverflow.fade,
                      ),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Número de casa:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor
                        ),
                      ),
                      SizedBox(width: 8),
                      Text(dir.hoseNumber),
                    ],
                  ),
                  SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Ref:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor
                        ),
                      ),
                      SizedBox(width: 8),
                      Text(dir.reference),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
          height: 2,
          width: MediaQuery.of(context).size.width - 32,
          decoration: BoxDecoration(
            color: Colors.black12
          ),
        ),
      ],
    );
  }
}
