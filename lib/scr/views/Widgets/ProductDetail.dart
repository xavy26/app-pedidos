import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pedidos_app/scr/models/product.model.dart';

class ProductDetail extends StatelessWidget {
  ProductModel product;
  ProductDetail({this.product});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 200,
          child: Image.network(
            '${product.image}',
            fit: BoxFit.fitHeight,
          ),
        ),
        Text(
          '${product.name}',
          textAlign: TextAlign.center,
          style: GoogleFonts.montserrat(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).accentColor,
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Row(
            children: [
              Text(
                'Precio:',
                style: GoogleFonts.montserrat(
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(width: 8.0),
              Text(
                '\$ ${product.cost}',
                softWrap: true,
                overflow: TextOverflow.clip,
                style: GoogleFonts.montserrat(
                    fontSize: 18
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Row(
            children: [
              Text(
                'Plato:',
                style: GoogleFonts.montserrat(
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(width: 8.0),
              Text(
                '${product.measure}',
                softWrap: true,
                overflow: TextOverflow.clip,
                style: GoogleFonts.montserrat(
                    fontSize: 18
                ),
              ),
            ],
          ),
        ),
        Text(
          'Descripción:',
          style: GoogleFonts.montserrat(
              fontSize: 18,
              fontWeight: FontWeight.bold
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Text(
            '${product.desc}',
            softWrap: true,
            overflow: TextOverflow.clip,
            style: GoogleFonts.montserrat(
                fontSize: 18
            ),
          ),
        ),
      ],
    );
  }
}
