import 'package:flutter/material.dart';
import 'package:pedidos_app/scr/models/shopcart.model.dart';

import 'package:toast/toast.dart';

import 'package:pedidos_app/scr/utilities/Constants.dart';

class ProductItem extends StatefulWidget {

  ShopcartModel item;
  ProductItem({this.item});

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {

  ShopcartModel item;

  @override
  void initState() {
    super.initState();
    item = this.widget.item;
    print(item.cant);
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 75,
              height: 75,
              child: CircleAvatar(
                backgroundImage: NetworkImage('${item.product.image}'),
              ),
            ),
            SizedBox(width: 8.0),
            Container(
              width: size.width/3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${item.product.name}',
                    softWrap: true,
                    overflow: TextOverflow.clip,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).accentColor
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    '\$ ${item.product.cost}',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 8.0),
            Container(
              width: 50,
              height: 50,
              child: CircleAvatar(
                backgroundColor: item.cant==0?Theme.of(context).accentColor:Colors.transparent,
                child: IconButton(
                  icon: Icon(
                    item.cant==0?Icons.add_shopping_cart:Icons.remove_shopping_cart,
                    color: item.cant==0?Colors.white:Theme.of(context).accentColor,
                  ),
                  onPressed: () {
                    setState(() {
                      item.cant==0?item.cant++:item.cant--;
                      item.cant==0?Constants.shopcart.remove(item):Constants.shopcart.add(item);
                      print('Carrito: ${Constants.shopcart.length}');
                    });
                    print(item.cant);
                    Toast.show(item.cant==0?'Quitado al carrito!':'Añadido al carrito!', context, duration: Toast.LENGTH_LONG);
                  },
                ),
              ),
            )
          ],
        ),
        Divider(
          color: Colors.black26,
        )
      ],
    );
  }
}
