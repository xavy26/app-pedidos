import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pedidos_app/scr/models/restaurat.model.dart';

class RateRest extends StatefulWidget {

  RestaurantModel rest;

  RateRest(this.rest);

  @override
  _RateRestState createState() => _RateRestState();
}

class _RateRestState extends State<RateRest> {

  bool one = false;
  bool two = false;
  bool three = false;
  bool four = false;
  bool five = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(32.0),
      child: Column(
        children: [
          Text(
              'Califique ${this.widget.rest.name}',
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              )
          ),
          SizedBox(height: 32.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    one = true;
                    two = false;
                    three = false;
                    four = false;
                    five= false;
                  });
                  print('One: $one');
                },
                child: Icon(
                  one?Icons.star:Icons.star_border,
                  color: Colors.amber,
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    one = true;
                    two = true;
                    three = false;
                    four = false;
                    five= false;
                  });
                  print('Two: $two');
                },
                child: Icon(
                  two?Icons.star:Icons.star_border,
                  color: Colors.amber,
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    one = true;
                    two = true;
                    three = true;
                    four = false;
                    five= false;
                  });
                  print('three: $three');
                },
                child: Icon(
                  three?Icons.star:Icons.star_border,
                  color: Colors.amber,
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    one = true;
                    two = true;
                    three = true;
                    four = true;
                    five= false;
                  });
                  print('Four: $four');
                },
                child: Icon(
                  four?Icons.star:Icons.star_border,
                  color: Colors.amber,
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    one = true;
                    two = true;
                    three = true;
                    four = true;
                    five = true;
                  });
                  print('Five: $five');
                },
                child: Icon(
                  five?Icons.star:Icons.star_border,
                  color: Colors.amber,
                ),
              ),
            ],
          ),
          SizedBox(height: 32.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 92.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Theme.of(context).accentColor,
                textStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                )
              ),
              onPressed: () {
                print('Califica');
              },
              child: Text(
                'Calificar'
              ),
            ),
          )
        ],
      ),
    );
  }
}
