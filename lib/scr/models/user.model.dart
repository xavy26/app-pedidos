import 'package:pedidos_app/scr/models/account.model.dart';
import 'package:pedidos_app/scr/models/direction.model.dart';

class UserModel {

  String id;
  String dni;
  String typeDni;
  String gender;
  String firstName;
  String lastName;
  String type;
  String birthdate;
  String photo;
  String phone;
  String cellphone;
  bool active;
  AccountModel account;
  List<DirectionModel> addresses;

  UserModel({this.id, this.dni, this.typeDni, this.gender, this.firstName,
    this.lastName, this.type, this.birthdate, this.photo, this.phone,
    this.cellphone, this.active, this.account, this.addresses});

  factory UserModel.fromJson(Map<String, dynamic> json){
    return UserModel(
      id: json['_id'],
      dni: json['dni_person'],
      typeDni: json['type_dni_person'],
      gender: json['gender_person'],
      firstName: json['first_name_person'],
      lastName: json['last_name_person'],
      type: json['type_person'],
      birthdate: json['birthdate_person'].toString().split('T')[0],
      photo: json['photo_person'],
      active: json['active_person'],
      account: AccountModel.fromJson(json['account']),
    );
  }

  @override
  String toString() {
    return 'PersonModel{id: $id, dni: $dni, typeDni: $typeDni, gender: $gender, '
        'firstName: $firstName, lastName: $lastName, type: $type, birthdate: $birthdate, '
        'photo: $photo, phone: $phone, cellphone: $cellphone, active: $active, '
        'account: $account, addresses: $addresses}';
  }

}
