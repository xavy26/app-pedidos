import 'package:pedidos_app/scr/models/direction.model.dart';
import 'package:pedidos_app/scr/models/product.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';

class RestaurantModel {
  String id;
  String name;
  bool state;
  String imgCover;
  String imgProfile;
  ListProducts products;
  ListDirections direcctions;

  RestaurantModel({this.id, this.name, this.state, this.imgCover,
      this.imgProfile, this.products, this.direcctions});

  factory RestaurantModel.fromJson(Map<String,dynamic> json) {

    var temp = DirectionModel.fromJson(json['address_shop']);
    List<DirectionModel> dir = [temp];
    ListDirections dirs = ListDirections(directions: dir);
    String id = json['_id'];
    List<ProductModel> prods = [];
    var auxProd = Constants.products.products.where((element) => element.shop == id);
    auxProd.forEach((element) => prods.add(element));
    print('id: $id');
    ListProducts products = ListProducts(prods);

    return RestaurantModel(
      id: json['_id'],
      name: json['name_shop'],
      state: json['active_shop'],
      imgCover: Constants.URL_DEBUG + json['image_shop'],
      imgProfile: Constants.URL_DEBUG + json['image_shop'],
      products: products,
      direcctions: dirs,
    );
  }

  @override
  String toString() {
    return 'RestaurantModel{id: $id, name: $name, state: $state, imgCover: $imgCover, '
        'imgProfile: $imgProfile, products: $products, direcctions: $direcctions}';
  }

}

class ListRestaurants {
  List<RestaurantModel> restaurants;

  ListRestaurants(this.restaurants);

  factory ListRestaurants.fromJson(List<dynamic> array) {
    List<RestaurantModel> restaurants = array.map((json) => RestaurantModel.fromJson(json)).toList();
    return ListRestaurants(restaurants);
  }

  @override
  String toString() {
    return 'ListRestaurants{restaurants: $restaurants}';
  }
}
