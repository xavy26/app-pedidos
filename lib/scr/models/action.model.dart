class ActionModel {

  String id;
  String icon;
  String name;
  String shortName;
  String description;
  bool active;

  ActionModel(this.id, this.icon, this.name, this.shortName, this.description,
      this.active);

  @override
  String toString() {
    return 'Action_Model{id: $id, icon: $icon, name: $name, shortName: $shortName, description: $description, active: $active}';
  }

}