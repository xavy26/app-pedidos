import 'package:pedidos_app/scr/utilities/Constants.dart';

class ProductModel {
  String id;
  bool active;
  String name;
  String desc;
  String size;
  String stok;
  double cost;
  String shop;
  String image;
  String measure;
  String shortDesc;
  List<String> tags;
  String crossedText;
  String nutricionalValue;


  ProductModel({
      this.stok,
      this.id,
      this.active,
      this.name,
      this.desc,
      this.size,
      this.cost,
      this.shop,
      this.image,
      this.measure,
      this.shortDesc,
      this.tags,
      this.crossedText,
      this.nutricionalValue});

  factory ProductModel.fromJson(Map<String,dynamic> json) {
    List<String> tags = [];
    return ProductModel(
      tags: tags,
      active: json['active_product'] as bool,
      id: json['_id'] as String,
      nutricionalValue: json['nutritional_value_product'] as String,
      desc:  json['description_product'] as String,
      shortDesc:  json['short_promotional_text_product'] as String,
      stok:  json['stok_product'] as String,
      measure:  json['measure_product'] as String,
      cost:  double.parse(json['cost_product'] as String),
      name:  json['name_product'] as String,
      shop:  json['shop'] as String,
      image: Constants.URL_DEBUG + (json['image_product'] as String)
    );
  }

  @override
  String toString() {
    return 'ProductModel{stok: $stok, id: $id, active: $active, name: $name, desc: $desc, '
        'size: $size, cost: $cost, shop: $shop, image: $image, measure: $measure, shortDesc: $shortDesc, '
        'tags: $tags, crossedText: $crossedText, nutricionalValue: $nutricionalValue}';
  }
}

class ListProducts {
  List<ProductModel> products;

  ListProducts(this.products);

  factory ListProducts.fromJson(List<dynamic> array) {
    List<ProductModel> products = array?.map((json) => ProductModel.fromJson(json))?.toList() ?? [];
    return ListProducts(products);
  }

  @override
  String toString() {
    return 'ListProducts{products: $products}';
  }
}
