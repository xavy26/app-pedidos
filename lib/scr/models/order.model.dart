import 'package:pedidos_app/scr/models/direction.model.dart';
import 'package:pedidos_app/scr/models/product.model.dart';
import 'package:pedidos_app/scr/models/user.model.dart';

class OrderModel {
  String id;
  String msg;
  String desc;
  int timeOut;
  String state;
  double paymentValue;
  UserModel user;
  DirectionModel dir;
  ListProducts products;

  OrderModel({this.id, this.msg, this.desc, this.timeOut, this.state, this.user,
      this.paymentValue, this.dir, this.products});

  factory OrderModel.fromJson(Map<String,dynamic> json) {
    return OrderModel(
      id: json[''],
      msg: json[''],
      desc: json[''],
      timeOut: json[''],
      state: json[''],
      paymentValue: json[''],
      products: ListProducts(json['']),
      user: UserModel.fromJson(json['']),
      dir: DirectionModel.fromJson(json['']),
    );
  }
}
