import 'package:pedidos_app/scr/models/action.model.dart';

class AccountModel {

  String id;
  String email;
  String password;
  List<ActionModel> actions;

  AccountModel({this.id, this.email, this.password, this.actions});

  factory AccountModel.fromJson(Map<String,dynamic> json) {
    return AccountModel(
      id: json['_id'],
      email: json['email_account'],
      password: json['password_account'],
    );
  }

  @override
  String toString() {
    return 'AccountModel{id: $id, email: $email, password: $password, actions: $actions}';
  }

}