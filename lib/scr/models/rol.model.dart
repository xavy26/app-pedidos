class RolModel {
  String id;
  bool state;
  String name;
  String desc;

  RolModel({this.id, this.name, this.desc, this.state});

  factory RolModel.fromJson(Map<String,dynamic> json) {
    return RolModel(
      id: json[''],
      name: json[''],
      desc: json[''],
      state: json[''],
    );
  }

  @override
  String toString() {
    return 'RolModel{state: $state, name: $name, desc: $desc}';
  }
}
