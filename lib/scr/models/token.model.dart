class TokenModel {

  String id;
  String token;
  String ip;
  String date;
  String random;
  bool active;

  TokenModel(this.id, this.token, this.ip, this.date, this.random, this.active);

  TokenModel.basic(this.id, this.token, this.active);

  @override
  String toString() {
    return 'Token{id: $id, token: $token, ip: $ip, date: $date, random: $random, active: $active}';
  }

  factory TokenModel.fromJson(Map<String, dynamic> json) {
    return TokenModel.basic(json['_id'], json['token'], json['active_token']);
  }
}