class DirectionModel {
  String id;
  String name;
  String city;
  String canton;
  String country;
  String province;
  String reference;
  String hoseNumber;
  String mainStreet;
  String secoundStreet;

  DirectionModel({
      this.id,
      this.name,
      this.city,
      this.canton,
      this.country,
      this.province,
      this.reference,
      this.mainStreet,
      this.hoseNumber,
      this.secoundStreet});

  factory DirectionModel.fromJson(Map<String,dynamic> json) {
    return DirectionModel(
      id: json['_id'],
      name: json['name_address'],
      city: json['city_address'],
      canton: json['canton_address'],
      country: json['country_address'],
      province: json['province_address'],
      reference: json['reference_address'],
      mainStreet: json['main_street_address'],
      hoseNumber: json['house_number_address'],
      secoundStreet: json['secondary_street_address']
    );
  }

  @override
  String toString() {
    return 'DirectionModel{id: $id, name: $name, city: $city, canton: $canton, '
        'country: $country, province: $province, reference: $reference, '
        'hoseNumber: $hoseNumber, mainStreet: $mainStreet, secoundStreet: $secoundStreet}';
  }

}

class ListDirections {

  List<DirectionModel> directions;

  ListDirections({this.directions});

  factory ListDirections.fromJson(List<dynamic> array) {
    List<DirectionModel> directions = array?.map((json) => DirectionModel.fromJson(json))?.toList() ?? [];
    return ListDirections(directions: directions);
  }

  @override
  String toString() {
    return 'ListDirections{directions: $directions}';
  }
}
