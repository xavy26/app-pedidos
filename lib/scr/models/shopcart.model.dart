import 'package:pedidos_app/scr/models/product.model.dart';

class ShopcartModel {

  int cant;
  ProductModel product;

  ShopcartModel({this.cant, this.product});

  @override
  String toString() {
    return 'ShopcartModel{cant: $cant, product: $product}';
  }
}