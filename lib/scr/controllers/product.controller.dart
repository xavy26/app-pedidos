import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:pedidos_app/scr/utilities/Constants.dart';
import 'package:pedidos_app/scr/models/product.model.dart';
import 'package:pedidos_app/scr/models/restaurat.model.dart';
import 'package:pedidos_app/scr/utilities/Utilities.dart';
import 'package:toast/toast.dart';

class ProductController{

  static Future<ListRestaurants> getRestaurants(BuildContext context) async {
    ListRestaurants restaurants;
    Uri url = Uri.parse(Constants.URL_SHOP_GET);
    final resp = await http.post(
      url,
      headers: Constants.headers,
      body: jsonEncode(<String,dynamic> {
        'token': Constants.token.token,
        'verify': Constants.token.id,
        'type_query': 2
      })
    );
    var res = jsonDecode(resp.body);
    var statusCode = resp.statusCode;
    var status = res['status'];
    if (statusCode == 200 && status == 'success') {
      restaurants = ListRestaurants.fromJson(res['data']);
      return restaurants;
    } else
      return null;
  }

  static Future<ListProducts> getProducts(BuildContext context) async {
    ListProducts products;
    Uri url = Uri.parse(Constants.URL_PRODUCT_GET);
    final resp = await http.post(
      url,
      headers: Constants.headers,
      body: jsonEncode(<String,dynamic>{
        'token': Constants.token.token,
        'verify': Constants.token.id,
        'type_query': 2
      })
    );
    var res = jsonDecode(resp.body);
    var statusCode = resp.statusCode;
    var status = res['status'];
    if (statusCode == 200 && status == 'success') {
      products = ListProducts.fromJson(res['data']);
      Constants.products = products;
      return products;
    } else
      return null;
  }

  static Future<void> sendOrder(BuildContext context) async {
    Uri url = Uri.parse(Constants.URL_ORDER_SEND);
    List<Map<String,dynamic>> order = [];
    Constants.shopcart.map((item) {
      order.add({
        'product': item.product.id,
        'quantity': item.cant,
      });
    });
    final resp = await http.post(
      url,
      headers: Constants.headers,
      body: jsonEncode(<String,dynamic>{
        'person': Constants.user.id,
        'address': Constants.dir.id,
        'type_query': 2,
        'valor_total': Utilities.totalOrder(),
        'product': order
      })
    );
    print('OrdReg: ${resp.body}');
    if (resp != null) {
      var statusCode = resp.statusCode;
      var body = jsonDecode(resp.body);
      var msg = body['message'];
      var status = body['status'];
      if (statusCode == 200 && status == 'success') {
        Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
      }
      Toast.show(msg, context, duration: Toast.LENGTH_LONG);
    }
  }

}