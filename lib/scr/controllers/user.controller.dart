import 'dart:convert';

import 'package:pedidos_app/scr/models/direction.model.dart';
import 'package:toast/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import 'package:pedidos_app/scr/models/token.model.dart';
import 'package:pedidos_app/scr/models/user.model.dart';
import 'package:pedidos_app/scr/utilities/Constants.dart';

class UserController {
  static Future<void> getToken(BuildContext context) async {
    Uri url = Uri.parse(Constants.URL_GET_TOKEN);
    final resp = await http.get(url);
    var statusCode = resp.statusCode;
    if (statusCode == 200) {
      String msg = json.decode(resp.body)['message'];
      print(msg);
      Constants.token = TokenModel.fromJson(json.decode(resp.body)['data']);
    } else {
      print('Error al obtener token');
    }
  }

  static Future<void> login(
      {BuildContext context, String email, String password}) async {
    Uri url = Uri.parse(Constants.URL_LOGIN);
    final resp = await http.post(
      url,
      headers: Constants.headers,
      body: jsonEncode(<String, dynamic>{
        'token': Constants.token.token,
        'verify': Constants.token.id,
        'email': email,
        'password': password,
      }),
    );
    var res = json.decode(resp.body);
    var statusCode = resp.statusCode;
    var status = res['status'];
    var msg = res['message'];
    if (statusCode == 200 && status == 'success') {
      Constants.user = UserModel.fromJson(res['data']);
      getDirs(context).then((dirs) {
        Constants.user.addresses = dirs.directions;
        Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
      });
    }
    Toast.show(msg, context, duration: Toast.LENGTH_LONG);
  }

  static Future<UserModel> register(
      BuildContext context, UserModel person) async {
    UserModel user;
    Uri url = Uri.parse(Constants.URL_REGISTER);
    await http
        .post(url,
            headers: Constants.headers,
            body: jsonEncode(<String, dynamic>{
              'token': Constants.token.token,
              'verify': Constants.token.id,
              'first_name_person': person.firstName,
              'last_name_person': person.lastName,
              'dni_person': person.dni,
              'birthdate_person': person.birthdate,
              'type_dni_person': person.typeDni,
              'gender_person': person.gender,
              'type_person': person.type,
              'email_account': person.account.email,
              'password_account': person.account.password,
              'photo_person': person.photo,
              'ext': '.jpg',
            }))
        .then((resp) {
      var statusCode = resp.statusCode;
      var res = json.decode(resp.body);
      var status = res['status'];
      var msg = res['message'];
      if (statusCode == 20 && status == 'success') {
        user = UserModel.fromJson(res['data']);
      } else {
        user = null;
      }
      Toast.show(msg, context, duration: Toast.LENGTH_LONG);
    });
    print(user);
    return user;
  }

  static Future<DirectionModel> regDir(
      BuildContext context, DirectionModel dir) async {
    Uri url = Uri.parse(Constants.URL_DIR_REG);
    await http
        .post(url,
            headers: Constants.headers,
            body: jsonEncode(<String, dynamic>{
              'token': Constants.token.token,
              'verify': Constants.token.id,
              'type_query': 2,
              'person': Constants.user.id,
              'name_address': dir.name,
              'country_address': dir.country,
              'province_address': dir.province,
              'canton_address': dir.canton,
              'city_address': dir.canton,
              'main_street_address': dir.mainStreet,
              'secondary_street_address': dir.secoundStreet,
              'house_number_address': dir.hoseNumber,
              'reference_address': dir.reference,
            }))
        .then((resp) {
      print('DirReg: ${jsonDecode(resp.body)}');
      var res = jsonDecode(resp.body);
      print(res);
      var statusCode = resp.statusCode;
      var msg = res['message'];
      var status = res['status'];
      if (statusCode == 200 && status == 'success') {
        dir = DirectionModel.fromJson(res['data']);
      } else {
        dir = null;
      }
      Toast.show(msg, context, duration: Toast.LENGTH_LONG);
    });
    return dir;
  }

  static Future<ListDirections> getDirs(BuildContext context) async {
    Uri url = Uri.parse(Constants.URL_DIR_GET);
    final resp = await http.post(url,
        headers: Constants.headers,
        body: jsonEncode(<String, dynamic>{
          'token': Constants.token.token,
          'verify': Constants.token.id,
          'type_query': 2,
          'person': Constants.user.id
        }));
    if (resp != null) {
      var res = jsonDecode(resp.body);
      var statusCode = resp.statusCode;
      var status = res['status'];
      var msg = res['message'];
      if (statusCode == 200 && status == 'success') {
        print('GET_DIRS: $msg');
        return ListDirections.fromJson(res['data']);
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}
