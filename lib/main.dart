import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:pedidos_app/scr/views/DetailOrderView.dart';

import 'package:pedidos_app/scr/views/HomeView.dart';
import 'package:pedidos_app/scr/views/InitView.dart';
import 'package:pedidos_app/scr/views/ProductView.dart';
import 'package:pedidos_app/scr/views/SelectDirView.dart';
import 'package:pedidos_app/scr/views/UserView.dart';
import 'package:pedidos_app/scr/views/LoginView.dart';
import 'package:pedidos_app/scr/views/RegisterDir.dart';
import 'package:pedidos_app/scr/views/ShopcartView.dart';
import 'package:pedidos_app/scr/views/DirectionsView.dart';
import 'package:pedidos_app/scr/views/RegisterUserView.dart';
import 'package:pedidos_app/scr/views/SplashScreenView.dart';
import 'package:pedidos_app/scr/views/RegisterRestaurantView.dart';

void main() => runApp(StartApp());

class StartApp extends StatefulWidget {
  @override
  _StartAppState createState() => _StartAppState();
}

class _StartAppState extends State<StartApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RapiNASA',
      home: SplashScreenView(),
      theme: ThemeData(
        // balck = 242A37
        // grey = F1F2F6
        accentColor: Color(0xFFFF8900),
        cupertinoOverrideTheme: CupertinoThemeData(
          primaryColor: Color(0xFFFF8900),
        ),
        primaryColor: Color(0xFFFFD428),
        primaryColorDark: Color(0xFF242A37),
        appBarTheme: AppBarTheme(
          backgroundColor: Color(0xFFFFD428),
          toolbarTextStyle: GoogleFonts.pacifico(
            fontWeight: FontWeight.bold,
            color: Color(0xFF242A37),
          ),
          centerTitle: true
        ),
        inputDecorationTheme: const InputDecorationTheme(
          labelStyle: TextStyle(color: Color(0xFFFF8900)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFFF8900)),
              borderRadius: BorderRadius.all(Radius.circular(25))),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFF242A37)),
              borderRadius: BorderRadius.all(Radius.circular(25)),
            )
        ),
        textTheme: TextTheme(
          headline1: GoogleFonts.pacifico(
            color: Color(0xFF242A37),
            fontSize: 28
          ),
          headline2: GoogleFonts.pacifico(
            color: Color(0xFFFFD428),
          ),
          // to appBar
          headline3: GoogleFonts.pacifico(
            color: Color(0xFF242A37),
            fontSize: 28,
          ),
          headline4: GoogleFonts.pacifico(
            color: Color(0xFF242A37),
          ),
          headline5: GoogleFonts.montserrat(
            color: Color(0xFFFFD428),
            fontWeight: FontWeight.bold
          ),
          headline6: GoogleFonts.montserrat(color: Color(0xFF242A37)),
        ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: Color(0xFF242A37)
        ),
      ),
      routes: <String, WidgetBuilder>{
        '/home': (context) => HomeView(),
        '/init': (context) => InitView(),
        '/user': (context) => UserView(),
        '/login': (context) => LoginView(),
        '/product': (context) => ProductView(),
        '/shopcart': (context) => ShopcartView(),
        '/detail': (context) => OrderDetailView(),
        '/select/dir': (context) => SelectDirView(),
        '/directions': (context) => DirectionsView(),
        '/register/dir': (context) => RegisterDirView(),
        '/register/user': (context) => RegisterUserView(),
        '/register/restaurant': (context) => RegisterRestaurantView(),
      },
    );
  }
}
